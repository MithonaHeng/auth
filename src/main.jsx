import React from 'react'
import ReactDOM from 'react-dom/client'
import {AuthInit} from './module/auth/components/AuthInit'
import { AuthProvider } from './module/auth/components/Auth'
import App from './App'
import 'bootstrap/dist/css/bootstrap.css'
import './index.css'
import { setUpAxios } from './module/auth/components/AuthHelper'

const root = ReactDOM.createRoot(document.getElementById('root'));
setUpAxios();
root.render(
  <AuthProvider>
  <AuthInit>
      <App/>
  </AuthInit>
</AuthProvider>
)
