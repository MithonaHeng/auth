import React from 'react'
import { useState } from 'react';
import { useAuth } from './Auth';
import '../../../assets/css/login.css'
import { login } from '../core/Request';
const Login = () => {
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const {saveAuth} = useAuth();

    const onLogin = (event) => {
        event.preventDefault();
        login(userName,password).then((response) => {
            setError("");
            saveAuth(response.data.token)
        }).catch((error) => {
            setError(error.response.data.message);
        })
    }
  return (
     <div className="container">
    <div className="row">`
        <div className="col-md-6 offset-md-3">
            <h2 className="text-center text-dark mt-5">Login Form</h2>
            <div className="text-center mb-5 text-dark">Made with bootstrap</div>
            <div className="card my-5">

                <form className="card-body cardbody-color p-lg-5">

                    <div className="text-center">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqvnbwwGrX50BfGE-MC1mhzEkEWXkKo2UNuBxywzkxHSyVeHx_K7UUdW-QtD4PvAJh6R8&usqp=CAU"
                             className="img-fluid profile-image-pic img-thumbnail rounded-circle my-3"
                             width="200px" alt="profile"/>
                    </div>

                    <div className="mb-3">
                        <input type="text"
                               className="form-control" id="Username" aria-describedby="emailHelp"
                               placeholder="User Name" 
                               onChange={(e) => setUserName(e.target.value)}
                               autoComplete='true'
                               />
                    </div>
                    <div className="mb-3">
                        <input type="password" className="form-control" id="password" placeholder="password"
                               onChange={(e) => setPassword(e.target.value) 
                               }
                               autoComplete='false'/>
                    </div>
                    <div className="text-center">
                        <button onClick={onLogin} 
                                className="btn btn-color px-5 mb-5 w-100 border-black bg-body-secondary">Login</button>
                    </div>
                    <div className="text-center text-danger">{error}</div>

                </form>
            </div>

        </div>
    </div>
</div>
  )
}

export default Login