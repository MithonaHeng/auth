import {useEffect, useState} from "react";
import {getListProduct,searchProduct,getCategories,getListProductByCategory} from "./core/Request";
import {useNavigate} from "react-router-dom";
import { Button, Form ,Badge} from "react-bootstrap";
import PaginationBasic from "./Pagination";

const ListProducts = () => {

    const [products,setProducts]=useState([]);
    const navigate=useNavigate();
    const [search,setSearch] = useState('');
    const [categories, setCategories] = useState([]);
    const [total, setTotal] = useState(0);
    const [active, setActive] = useState("All");

    useEffect(()=>{
            getListProduct(100).then((response)=>{
                setProducts(response.data.products)
            })
    },[]);

    const onSearch =()=>{
        searchProduct({q:search}).then((response)=>{
            setProducts(response.data.products);
        });
    };


    const limit = 8;

    useEffect(() => {
        getListProduct(limit).then((response) => {
            setProducts(response.data.products);
            setTotal(response.data.total);
        });

        getCategories().then((response) => {
            setCategories(response.data);
        })
    }, []);

            // categories 
    useEffect(() => {
        active === "All" ?
            getListProduct(limit).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);
            })
            : getListProductByCategory(limit, active).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);

            });
    }, [active]);

   return <div className=" container">
                    {/* search bar */}
            <div className="search container d-flex justify-content-center">
                <Form action="" className=" w-50 d-flex gap-3 py-4">
                    <Form.Control
                        type="search"
                        placeholder='Search' 
                        aria-label="Search"
                        onChange={(e)=>setSearch(e.target.value)}
                
                    />
                    <Button onClick={onSearch}>Search</Button>
                </Form>
            </div>

                    {/* categories */}
                    <div className="px-5">
                        <div className="d-flex overflow-x-scroll pb-3">
                            <Badge onClick={() => setActive("All")} className="px-4 py-2 fs-6 mx-2" bg={active === "All" ? "primary" : "secondary"}>All</Badge>
                            {categories.map((item, index) =>
                                <Badge onClick={() => setActive(item)} key={index} className="px-4 py-2 fs-6 mx-2 " bg={active === item ? "primary" : "secondary"}>{item}</Badge>)}
                        </div>
                    </div>

            <div className="d-flex flex-wrap justify-content-center productDetail">
                        {/* product  */}
                {products.map((product,index)=>{
                    return <div onClick={()=>navigate(`product/${product.id}`)} className="mx-2 border rounded-2 my-2" style={{
                    }} key={index}>
                        <img style={{
                            height:"200px",
                            width:"300px"
                        }} className="" src={product.thumbnail} alt=""/>
                            <div className="py-3">
                            <h4 className="border-top text-start ps-3">{product.title}</h4>
                            <div className="price d-flex ps-3 align-items-center">
                                <h5 className=" text-danger">{(product.price-((product.price*product.discountPercentage)/100)).toFixed(2)}$</h5>
                                <h6 className=" ps-2 text-decoration-line-through" >{product.price}$</h6>
                            </div>
                            </div>
                    </div>
                })}
        </div>
        
                <div className="d-flex justify-content-center my-3">
                    <PaginationBasic onChangePage={(page) => {
                        getListProduct(limit, (page - 1) * limit).then((response) => {
                            setProducts(response.data.products);
                            setTotal(response.data.total);
                        });

                    }} total={total / limit} />
                </div>

</div>
}

export default ListProducts;