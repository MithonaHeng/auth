import axios from "axios";
import {getAuth} from "../../auth/components/AuthHelper";


const getListProduct = (limit = 10, skip = 0) => {
    return axios.get("/products", {
        params: { limit, skip }
    });
}

const getProductById = (id) => {
    return axios.get(`https://dummyjson.com/products/${id}`, {
        headers: {
            "Authorization": `Bearer ${getAuth()}`
        },
    });
}

const searchProduct = (params) => {
    return axios.get("/products/search", {
        params: params
    });
}

const getCategories = () => {
    return axios.get("/products/categories");
}

const getListProductByCategory = (limit = 10, path = "") => {
    return axios.get(`/products/category/${path}`, {
        params: { limit }
    });
}

export {getListProduct, getProductById,searchProduct, getCategories,getListProductByCategory}