import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {getProductById} from "./core/Request";
import { Badge,Button, Carousel } from "react-bootstrap";
import { Rating } from 'react-simple-star-rating'
import Image from 'react-bootstrap/Image';

const ProductDetail = () => {
    const {id} = useParams();
    const [product, setProduct] = useState({});

    useEffect(() => {
        getProductById(id).then((response)=>{
            setProduct(response.data);
        })
    }, [id]);
    console.log(product);
    if (!product.id){
        return  <div className="text-center fs-6">
            Loading...
        </div>
    }

    return <div className="container product_detail d-grid">
        <div className="row">

            <div className="col-lg-6 col-md-12 col-sm-12 ">
                <div className="thumnail mt-3">
                    <img className="img-fluid w-100 h-75 object-fit-cover " src={product.thumbnail} alt=""/>
                </div>
                <div className="image my-5 w-75 d-flex justify-content-center">
                    <Carousel fade >
                    {product.images.map((images,index)=>{
                        return (
                              <Carousel.Item key={index}>
                              <Image height={200} src={images} alt="images-item" />
                            </Carousel.Item>)
                    })}
                    </Carousel>
                </div>
            </div>

            <div className="col-lg-6 col-md-12 col-sm-12">
                <div className=" my-5 ms-5">
                <h1 className=" mb-3">  {product.title}</h1>
                <div className=" d-flex mb-5">
                    <Rating initialValue={product.rating} size={25}/>
                    <h5 className=" text-center ps-3 m-0 align-self-center">{product.rating}</h5>
                </div>
                <div className="price d-flex mb-3">
                    <h3>
                    <Badge bg="danger">{(product.price-((product.price*product.discountPercentage)/100)).toFixed(2)}$</Badge>
                    </h3>
                    <h3 className=" ms-3 mb-0 text-decoration-line-through text-danger" >
                        {product.price}$
                    </h3>
                </div>
                <p className=" mb-3">{product.description}</p>
                <hr />
                <Button variant="warning" className=" px-5">Add to cart</Button>
                </div>
            </div>
        </div>
    </div>
}
export default ProductDetail;