
import {useAuth} from "../../auth/components/Auth";
import { Nav ,Navbar, Container } from "react-bootstrap";
import { Link, Outlet } from "react-router-dom";


function Header() {
    const {logout}=useAuth();
    return <>
             <Navbar bg="primary" data-bs-theme="dark">
        <Container>
          <Navbar.Brand to="/">Auth</Navbar.Brand>
          <Nav className="me-auto gap-5">
            <Link className=" text-decoration-none text-white" to="/">Home</Link>
            <Link className=" text-decoration-none text-white" to="blog">Blog</Link>
            <Link className=" text-decoration-none text-white" to="contact">Contact</Link>
          </Nav>
          <button 
          className=" btn btn-outline-danger bg-danger text-white"
          onClick={() => {
              logout()
            }}>
                Logout
            </button>
        </Container>
      </Navbar>
    </>
}
export default Header;