
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import HomePage from "./module/home/Home";
import BlogPage from "./module/blog/Blog";
import ContactUsPage from "./module/contact/Contact";
import ErrorPage from "./module/error/Error";
import Layout from "./module/layout/Layout";
import "bootstrap/dist/css/bootstrap.min.css"
import Login from "./module/auth/components/Login";
import {useAuth} from "./module/auth/components/Auth";
import ProductDetail from "./module/product/ProductDetail";
const App = () => {


  const {auth} = useAuth();

  return (
    <BrowserRouter>
      <Routes>
        {auth ?
          <Route path="/" element={<Layout />}>
            <Route index element={<HomePage />} />
            <Route exact path="blog" element={<BlogPage />} />
            <Route exact path="contact" element={<ContactUsPage />} />
            <Route path="product/:id" element={<ProductDetail />} />
            <Route path="*" element={<Navigate to="/" />}></Route>
          </Route>
          :
          <>
            <Route index path="auth/login" element={<Login />} />
            <Route path="*" element={<Navigate to="/auth/login" />}></Route>
          </>
        }
        <Route path="error" element={<ErrorPage />} />

      </Routes>
    </BrowserRouter>
  );
};


export default App;
